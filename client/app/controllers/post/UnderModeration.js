angular.module("mobile-events").controller("UnderModerationListCtrl", ['$scope', '$rootScope', '$collection', '$location',
    function ($scope, $rootScope, $collection, $location) {
        $collection(UnderModeration).bind($scope, 'underModerationPosts');
        $collection(Category).bind($scope, 'categories');

        $scope.removePost = function (id) {
            UnderModeration.remove({_id: id}, function (error) {
                if (error) {
                    console.log(error);
                }
            });
        };

        $scope.editPost = function (post) {
            $rootScope.$broadcast('current-post', post);
        };

        $scope.approvePost = function (post) {
            Meteor.call('approvePost', post._id, $location.host(), function (err, status) {
                if(err) {
                    console.log(err);
                }
            });
        };
    }]);