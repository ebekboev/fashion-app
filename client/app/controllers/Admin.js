angular.module("mobile-events").controller("AdminCtrl", ['$scope', '$subscribe',
    function ($scope, $subscribe) {
        $scope.currentTable = 'under-moderation';
        $scope.setCurrentTable = function (table) {
            $scope.currentTable = table;
        };
        $subscribe.subscribe('activeUser').then(function () {
            $scope.isAdmin = function () {
                return Roles.userIsInRole(Meteor.userId(), 'super-admin');
            };
            $scope.isModerator = function () {
                return Roles.userIsInRole(Meteor.userId(), 'moderator');
            };
            if ($scope.isAdmin()) {
                $scope.currentTable = 'category';
            }
        });
    }]);