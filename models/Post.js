Post = new Mongo.Collection('Post');

Post.allow({
    insert: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    },
    update: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    },
    remove: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    }
});

Meteor.methods({
    voteForPost: function (postId) {
        var post = Post.findOne(postId);
        if (this.userId && post != undefined) {
            post.votes.indexOf(this.userId) > -1 ?
                Post.update(postId, {$pull: {votes: this.userId}}) : Post.update(postId, {$push: {votes: this.userId}});
        }
    },
    approvePost: function (id, host) {
        var postToApprove = UnderModeration.findOne(id);
        if (Roles.userIsInRole(this.userId, ['super-admin', 'moderator']) && postToApprove) {
            var postId = Post.insert(postToApprove);
            UnderModeration.remove({_id: id});
            Meteor.call('notifySubscribers', postId, host);
        }
    },
    addPostComment: function (postId, parentIndex, childIndex, target, message) {
        var post = Post.findOne(postId);
        if (this.userId && post != undefined) {
            if (parentIndex == null && childIndex == null && target == null) {
                Post.update(postId, {
                    $push: {
                        comments: {
                            $each: [{
                                message: message,
                                created: (new Date()).getTime(),
                                author: this.userId,
                                comments: []
                            }]
                        }
                    }
                });
            } else if (childIndex > -1) {
                var comment = {
                    message: message,
                    created: (new Date()).getTime(),
                    author: this.userId
                };
                Post.update({
                    _id: postId,
                    "comments.comments": {
                        $elemMatch: {
                            created: target.created,
                            author: target.author,
                            message: target.message
                        }
                    }
                }, {
                    $push: {
                        "comments.$.comments": {
                            $each: [comment],
                            $position: childIndex + 1
                        }
                    }
                });
            } else {
                var comment = {
                    message: message,
                    created: (new Date()).getTime(),
                    author: this.userId
                };
                Post.update({
                    _id: postId,
                    comments: {
                        $elemMatch: {
                            created: target.created,
                            author: target.author,
                            message: target.message
                        }
                    }
                }, {
                    $push: {
                        "comments.$.comments": {
                            $each: [comment],
                            $position: 0
                        }
                    }
                });
            }
        }
    },
    getPostsCount: function () {
        return Post.find().count();
    }
});
