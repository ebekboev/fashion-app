angular.module("mobile-events").controller("PostsListCtrl", ['$scope', '$collection',
    function ($scope, $collection) {
        $collection(Post).bind($scope, 'posts');

        $scope.removePost = function (post) {
            Post.remove({_id: post._id});
        };
    }]);
