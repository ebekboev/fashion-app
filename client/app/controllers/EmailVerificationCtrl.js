angular.module("mobile-events").controller("EmailVerificationCtrl", ['$scope', '$stateParams', '$location', '$timeout',
    function ($scope, $stateParams, $location, $timeout) {
        Meteor.call('verifyNewEmail', $stateParams.userId, $stateParams.verifyId, function (error, status) {
            if (error) {
                console.log(error);
            } else {
                if (status) {
                    $scope.message = "Congratulations! Your email is successfully verified.";
                    $scope.$apply();
                    $timeout(function () {
                        $location.path("/");
                    }, 3000);
                } else {
                    $scope.message = "User not found. Redirecting...";
                    $scope.$apply();
                    $timeout(function () {
                        $location.path("/");
                    }, 3000);
                }
            }
        });
    }]);
