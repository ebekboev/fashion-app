angular.module("mobile-events").controller("EditUnderModerationCtrl", ['$scope', '$rootScope', '$collection',
    function ($scope, $rootScope, $collection) {
        $rootScope.$on('current-post', function (error, post) {
            if(!$scope.postToApprove) {
                $collection(UnderModeration).bindOne($scope, "postToApprove", post._id);
            }
            $scope.post = post;
            var created = moment(post.created);
            console.log(created.format("MM/DD/YYYY"));
            $("#datepicker").datepicker();
            $( "#datepicker" ).datepicker( "setDate" , created.format("MM/DD/YYYY") );
        });

        $scope.method = 'url';

        $scope.updatePost = function () {
            $('body').addClass("loading");
            if ($scope.method == 'pc') {
                $scope.uploadImageFromPc(updateCallback);
            } else {
                if ($scope.post.image.original == $scope.postToApprove.image.original) {
                    updateCallback();
                } else {
                    $scope.post.image.small = $scope.post.image.original;
                    $scope.post.image.thumb = $scope.post.image.original;
                    updateCallback();
                }
            }
        };

        $scope.uploadImageFromPc = function (callback) {
            var file = $scope.thumbnail;
            if (!file.type.match('image.*')) {
                $scope.thumbnail = null;
                $('#edit-image-upload').val("");
                alert('Select an image, please!');
                return;
            } else {
                Original.insert(file, function (err, fileObj) {
                    Deps.autorun(function (computation) {
                        var file = Original.findOne(fileObj._id);
                        if (file.hasStored('small') && file.hasStored('thumb') && file.hasStored('original')) {
                            $scope.post.image.small = '/cfs/files/original/' + fileObj._id + "?store=small";
                            $scope.post.image.thumb = '/cfs/files/original/' + fileObj._id + "?store=thumb";
                            $scope.post.image.original = '/cfs/files/original/' + fileObj._id + "?store=original";
                            callback();
                            computation.stop();
                        }
                    });
                });
            }
        };

        var updateCallback = function () {
            $scope.thumbnail = null;
            $('#edit-image-upload').val("");
            UnderModeration.update({_id: $scope.post._id}, {
                $set: {
                    title: $scope.post.title,
                    category: $scope.post.category,
                    source: $scope.post.source,
                    created: new Date($("#datepicker").datepicker("getDate")).getTime(),
                    image: {
                        small: $scope.post.image.small,
                        thumb: $scope.post.image.thumb,
                        original: $scope.post.image.original
                    }
                }
            });
            $scope.artist = null;
            $("#edit-post-modal").modal('hide');
            $('body').removeClass("loading");
        };
    }]);
