SubscribedEmails = new Mongo.Collection('SubscribedEmails');

SubscribedEmails.allow({
    insert: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    },
    update: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    },
    remove: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    }
});

Meteor.methods({
    subscribe: function (email, host) {
        var emails = SubscribedEmails.find({email: email}).fetch();
        if (emails.length > 0) {
            return "Already subscribed!";
        } else {
            var unsubscribeId = Random.id(100);
            SubscribedEmails.insert(
                {
                    email: email,
                    unsubscribeId: unsubscribeId
                }
            );
            var unsubscribeUrl = host + "/unsubscribe/" + unsubscribeId;
            Meteor.call("sendEmail", email, "support@fits.io", "Hello!",
                "<h1>You have successfully subscribed to fits.io community!!</h1>" + "<br/>" +
                "<h4>To unsubcribe, please follow the link below</h4>" + "<br/>" +
                "<h3><a href=" + unsubscribeUrl + ">Unsubscribe from fits.io</a></h3>"
            );
        }
    },
    unsubscribe: function (unsubscribeId) {
        var subscriber = SubscribedEmails.findOne({unsubscribeId: unsubscribeId});
        if (subscriber) {
            Meteor.call('removeSubscriber', subscriber._id);
            return true;
        } else {
            return false;
        }
    },
    removeSubscriber: function (subscriberId) {
        SubscribedEmails.remove({_id: subscriberId});
    }
});
