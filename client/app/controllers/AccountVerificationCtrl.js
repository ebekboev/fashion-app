angular.module("mobile-events").controller("AccountVerificationCtrl", ['$scope', '$stateParams', '$location', '$timeout',
    function ($scope, $stateParams, $location, $timeout) {
        Meteor.call('verifyAccount', $stateParams.userId, $stateParams.verifyId, function (error, status) {
            if (error) {
                console.log(error);
            } else {
                if (status) {
                    $scope.message = "Your account is successfully activated. Please Sign In.";
                    $scope.$apply();
                    $timeout(function () {
                        $location.path("/");
                    }, 3000);
                } else {
                    $scope.message = "User not found. Redirecting...";
                    $scope.$apply();
                    $timeout(function () {
                        $location.path("/");
                    }, 3000);
                }
            }
        });
    }]);
