angular.module("mobile-events").config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
    function ($urlRouterProvider, $stateProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $stateProvider
            .state('home', {
                url: '/',
                template: UiRouter.template('main.html'),
                controller: 'MainCtrl'
            })
            .state('home.post', {
                url: 'posts/:id',
                template: UiRouter.template('post-details.html'),
                controller: 'PostDetailsCtrl'
            })
            .state('admin', {
                url: '/admin',
                template: UiRouter.template('admin.html'),
                controller: 'AdminCtrl'
            })
            .state('unsubscribe', {
                url: '/unsubscribe/:id',
                template: UiRouter.template('unsubscribe.html'),
                controller: 'UnsubscriptionCtrl'
            })
            .state('subscribe', {
                url: '/subscribe/:id',
                template: UiRouter.template('subscribe.html'),
                controller: 'SubscriptionCtrl'
            })
            .state('verify-account', {
                url: '/users/:userId/verify-account/:verifyId',
                template: UiRouter.template('verify-account.html'),
                controller: 'AccountVerificationCtrl'
            })
            .state('verify-email', {
                url: '/users/:userId/verify-new-email/:verifyId',
                template: UiRouter.template('verify-email.html'),
                controller: 'EmailVerificationCtrl'
            })
            .state('forgot-password', {
                url: '/forgot-password',
                template: UiRouter.template('forgot-password.html'),
                controller: 'PasswordRestoreCtrl'
            })
            .state('verify-password', {
                url: '/users/:userId/verify-password/:verifyId',
                template: UiRouter.template('verify-password.html'),
                controller: 'PasswordVerificationCtrl'
            });

        $urlRouterProvider.otherwise("/");
    }]);
