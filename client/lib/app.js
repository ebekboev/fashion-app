angular.module('mobile-events', ['angular-meteor', 'ui.router']);

Meteor.startup(function () {
    angular.bootstrap(document, ['mobile-events']);

    Original = new FS.Collection("original", {
        stores: [new FS.Store.FileSystem("original")]
    });

    Meteor.subscribe("AllUsers");
    Meteor.subscribe("Roles");
    Meteor.subscribe("SubscribedEmails");
    Meteor.subscribe('Post');
    Meteor.subscribe('UnderModerationPosts');
    Meteor.subscribe('Category');
    Meteor.subscribe('Original');
});
