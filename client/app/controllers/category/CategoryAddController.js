angular.module('mobile-events').controller("CategoryAddCtrl", ['$scope', '$collection',
    function ($scope) {

        $scope.method = 'pc';

        $scope.addCategory = function () {
            $('body').addClass("loading");
            if ($scope.method == 'pc' && $scope.avatar) {
                $scope.uploadImageFromPc(saveCallback);
            } else {
                $scope.newCategory.image.thumb = $scope.newCategory.image.original;
                $scope.newCategory.image.small = $scope.newCategory.image.original;
                saveCallback();
            }
        };

        $scope.newCategory = {
            title: null,
            description: null,
            image: {
                small: null,
                thumb: null,
                original: null
            }
        };

        $scope.isInputValid = function() {
            return !$scope.newCategory.description || !$scope.newCategory.title ||
                !$scope.newCategory.image.original && $scope.method == "url" ||
                !$scope.avatar && $scope.method == "pc";
        }

        var clearViewModel = function () {
            $scope.newCategory = {
                title: null,
                description: null,
                image: {
                    small: null,
                    thumb: null,
                    original: null
                }
            };
            $('#add-artist-modal').modal('hide');
            $('body').removeClass("loading");
        };

        var saveCallback = function () {
            $('#add-image-upload').val("");
            $scope.avatar = null;
            Category.insert($scope.newCategory);
            clearViewModel();
        };

        $scope.uploadImageFromPc = function (callback) {
            var file = $scope.avatar;
            if (!file.type.match('image.*')) {
                alert('Select an image, please!');
                return;
            } else {
                Original.insert(file, function (err, fileObj) {
                    Deps.autorun(function (computation) {
                        var file = Original.findOne(fileObj._id);
                        if (file.hasStored('small') && file.hasStored('thumb') && file.hasStored('original')) {
                            $scope.newCategory.image.original = "/cfs/files/original/" + fileObj._id + "?store=original";
                            $scope.newCategory.image.thumb = "/cfs/files/original/" + fileObj._id + "?store=thumb";
                            $scope.newCategory.image.small = "/cfs/files/original/" + fileObj._id + "?store=small";
                            callback();
                            computation.stop();
                        }
                    });
                });
            }
        };
    }]);
