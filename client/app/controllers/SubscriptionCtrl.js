angular.module("mobile-events").controller("SubscriptionCtrl", ['$scope', '$stateParams', '$location', '$timeout',
    function ($scope, $stateParams, $location, $timeout) {
        Meteor.call('verifySubscribe', $stateParams.id, $location.host(), function (error, status) {
            if (error) {
                console.log(error);
            } else {
                if (status) {
                    $scope.message = "You have successfully subscribed!";
                    $scope.$apply();
                    $timeout(function () {
                        $location.path("/");
                    }, 2000);
                } else {
                    $scope.message = "If you want to subscribe, please provide your email on homepage. Redirecting...";
                    $scope.$apply();
                    $timeout(function () {
                        $location.path("/");
                    }, 3000);
                }
            }
        });
    }]);