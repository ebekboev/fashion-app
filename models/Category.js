Category = new Mongo.Collection('Category');

Category.allow({
    insert: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    },
    update: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    },
    remove: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    }
});