angular.module("mobile-events").controller("ProfileCtrl", ["$scope", "$rootScope",
    function ($scope, $rootScope) {
        $rootScope.$on("current-user", function (error, user) {
            $scope.user = {
                name: user.profile.name,
                email: user.emails[0].address,
                password: null,
                newPassword: null,
                thumbnail: user.profile.thumbnail
            };

            $scope.method = 'url';
        });

        var updateCallback = function () {
            if ($rootScope.currentUser.emails[0].address == $scope.user.email) {
                Meteor.users.update($rootScope.currentUser._id, {
                    $set: {
                        'profile.name': $scope.user.name,
                        'profile.thumbnail': $scope.user.thumbnail
                    }
                }, function () {
                    if ($scope.changeStatus) {
                        Accounts.changePassword($scope.user.password, $scope.user.newPassword, function (error) {
                            $scope.changeStatus = false;
                            $scope.avatar = null;
                            $('#add-image-upload').val("");
                            $('body').removeClass("loading");
                            $('#modalChangeProfile').modal('hide');
                        });
                    } else {
                        $scope.avatar = null;
                        $('#add-image-upload').val("");
                        $('body').removeClass("loading");
                        $('#modalChangeProfile').modal('hide');
                    }
                });
            } else {
                Meteor.users.update($rootScope.currentUser._id, {
                    $set: {
                        'profile.unverifiedEmail': $scope.user.email,
                        'profile.name': $scope.user.name,
                        'profile.thumbnail': $scope.user.thumbnail
                    }
                }, function () {
                    if ($scope.changeStatus) {
                        Accounts.changePassword($scope.user.password, $scope.user.newPassword, function (error) {
                            $('body').removeClass("loading");
                            $('#modalChangeProfile').modal('hide');
                            $scope.changeStatus = false;
                            $scope.avatar = null;
                            $('#add-image-upload').val("");
                        });
                    } else {
                        $('body').removeClass("loading");
                        $('#modalChangeProfile').modal('hide');
                    }
                    Meteor.call("sendVerificationMessage");
                    $('#verify-confirmation').modal('show');
                });
            }
        };

        var checkCallback = function () {
            var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (regex.test($scope.user.email)) {
                Meteor.call("checkUserEmail", $scope.user.email, function (error, status) {
                    if (status) {
                        $('#existantEmail').hide();
                        if ($scope.method == 'pc') {
                            uploadImageFromPc(updateCallback);
                        } else {
                            updateCallback();
                        }
                    } else {
                        $('body').removeClass("loading");
                        $('#existantEmail').show();
                    }
                });
            } else {
                $('body').removeClass("loading");
                $('#emailFormatError').show();
            }
        };

        $scope.updateUser = function () {
            $('body').addClass("loading");
            if ($scope.changeStatus) {
                Meteor.call("checkUserPassword", $scope.user.password, function (error, status) {
                    if (status) {
                        checkCallback();
                    } else {
                        $('body').removeClass("loading");
                        $('#wrongPassword').show();
                    }
                });
            } else {
                checkCallback();
            }
        };

        var uploadImageFromPc = function (callback) {
            var file = $scope.avatar;
            if (!file.type.match('image.*')) {
                alert('Select an image, please!');
                return;
            } else {
                Original.insert(file, function (err, fileObj) {
                    Deps.autorun(function (computation) {
                        var file = Original.findOne(fileObj._id);
                        if (file.hasStored('small') && file.hasStored('thumb') && file.hasStored('original')) {
                            $scope.user.thumbnail = "/cfs/files/original/" + fileObj._id + "?store=small";
                            callback();
                            computation.stop();
                        }
                    });
                });
            }
        };
    }]);