angular.module("mobile-events").service("CurrentPost", [
    function () {
        var currentPost = null;

        return {
            getCurrentPost: function (callback) {
                callback(currentPost);
            },
            setCurrentPost: function (post) {
                currentPost = post;
            }
        };
    }]);
