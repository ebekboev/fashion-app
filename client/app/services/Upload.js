angular.module("mobile-events").directive("fileread", [
	function () {
		return {
			scope: {
				fileread: "="
			},
			link: function (scope, element, attributes) {
				element.bind("change", function (changeEvent) {
					scope.$apply(function () {
						scope.fileread = changeEvent.originalEvent.target.files[0];
					});
				});
			}
		}
	}]);