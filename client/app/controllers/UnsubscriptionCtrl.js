angular.module("mobile-events").controller("UnsubscriptionCtrl", ['$scope', '$stateParams', '$location', '$timeout',
    function ($scope, $stateParams, $location, $timeout) {
        Meteor.call('unsubscribe', $stateParams.id, function (error, status) {
            if (error) {
                console.log(error);
            } else {
                if (status) {
                    $scope.message = "You have successfully unsubscribed!";
                    $scope.$apply();
                    $timeout(function () {
                        $location.path("/");
                    }, 2000);
                } else {
                    $scope.message = "Subscriber not found!";
                    $scope.$apply();
                    $timeout(function () {
                        $location.path("/");
                    }, 2000);
                }
            }
        });
    }]);