Meteor.methods({
    authenticate: function (userEmail, userPassword) {
        var users = Meteor.z;
        check(userPassword, String);

        for (user in users) {
            var emails = users[user].emails;
            for (email in emails) {
                if (emails[email].address == userEmail) {
                    var digest = Package.sha.SHA256(userPassword);
                    var password = {digest: digest, algorithm: 'sha-256'};
                    var result = Accounts._checkPassword(users[user], password);
                    if (result.error == null) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }
});