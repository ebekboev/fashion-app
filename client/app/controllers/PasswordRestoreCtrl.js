angular.module("mobile-events").controller("PasswordRestoreCtrl", ['$scope', '$location', '$timeout',
    function ($scope, $location, $timeout) {
        $scope.isEmailValid = function (email) {
            return checkEmail(email);
        };

        var checkEmail = function (email) {
            var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return regex.test(email);
        };

        $scope.restore = function (email) {
            $('#restore_email').prop('disabled', true);
            Meteor.call("sendPasswordRestoreMessage", email, function (err, result) {
                if (err) {
                    console.log(err);
                }
                if (result) {
                    $timeout(function () {
                        $location.path("/");
                    }, 1000);
                } else {
                    $('#emailNotFound').show();
                    $('#restore_email').prop('disabled', false);
                }
            });
        };

        $scope.goToHomePage = function () {
            $location.path('/');
        };

        $scope.hideErrors = function () {
            $('.warning').hide();
        };
    }]);
