UnverifiedSubscribers = new Mongo.Collection('UnverifiedSubscribers');

UnverifiedSubscribers.allow({
    insert: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    },
    update: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    },
    remove: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin']) ? true : false;
    }
});

Meteor.methods({
    verifySubscribe: function (verifyId, host) {
        var unverified = UnverifiedSubscribers.findOne({verifyId: verifyId});
        if(unverified) {
            Meteor.call('subscribe', unverified.email, host);
            UnverifiedSubscribers.remove({_id: unverified._id});
            return true;
        } else {
            return false;
        }
    }
});
