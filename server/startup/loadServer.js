Meteor.startup(function () {
    process.env.MAIL_URL = "smtp://postmaster@sandbox3c32b0e2fb9e48bc96066324bdf97e5e.mailgun.org:23b996b2ab6e02fc410f4de20f584738@smtp.mailgun.org:587/";

    Meteor.methods({
        createNewUser: function (email, password, name) {
            var userId = Accounts.createUser({
                email: email,
                password: password,
                profile: {thumbnail: null, name: name}
            });
            Roles.addUsersToRoles(userId, 'general');
        },
        removeUser: function (userToRemove) {
            var user = Meteor.users.findOne(userToRemove);
            if (Roles.userIsInRole(this.userId, 'super-admin') && this.userId != userToRemove && user) {
                Post.remove({author: userToRemove});
                Post.update({}, {$pull: {votes: userToRemove}});

                Post.update({
                        comments: {
                            $elemMatch: {
                                author: userToRemove
                            }
                        }
                    },
                    {
                        "$pull": {
                            comments: {
                                author: userToRemove
                            }
                        }
                    },
                    {
                        multi: true
                    }
                );
                Post.update(
                    {"comments.comments.author": userToRemove},
                    {
                        "$pull": {
                            "comments.$.comments": {
                                author: userToRemove
                            }
                        }
                    },
                    {
                        "multi": true
                    }
                );
                var posts = Post.find().fetch();
                for(var post in posts) {
                    var postComments = posts[post].comments;
                    for(var outerComment in postComments) {
                        var innerComments = postComments[outerComment].comments;
                        for(var innerComment in innerComments) {
                            if(innerComments[innerComment].author == userToRemove) {
                                Post.update(
                                    {"comments.comments.author": userToRemove},
                                    {
                                        "$pull": {
                                            "comments.$.comments": {
                                                author: userToRemove
                                            }
                                        }
                                    },
                                    {
                                        "multi": true
                                    }
                                );
                                break;
                            }
                        }
                    }
                }
                Meteor.users.remove({_id: userToRemove});
            } else {
                return 'You can not remove yourself!';
            }
        },
        setAsModerator: function (userId) {
            if (Roles.userIsInRole(this.userId, 'super-admin')) {
                Roles.addUsersToRoles(userId, 'moderator');
            } else {
                return 'You have not enough right to proceed this request!';
            }
        },
        setAsGeneralUser: function (userId) {
            if (Roles.userIsInRole(this.userId, 'super-admin')) {
                Roles.removeUsersFromRoles(userId, 'moderator')
            } else {
                return 'You have not enough right to proceed this request!';
            }
        },
        sharePostViaEmail: function (email, postId) {
            var subscribeUrl;
            var post = Post.findOne(postId);
            var currentUser = Meteor.user();
            var currentCategory = Category.findOne(post.category);
            var subject = "fits " + currentCategory.Title + " recommendation from " + currentUser.profile.name + "!";
            var unverified = UnverifiedSubscribers.findOne({email: email});
            if (unverified) {
                subscribeUrl = Meteor.absoluteUrl("subscribe/" + unverified.verifyId);
            } else {
                var verifyId = Random.id(100);
                UnverifiedSubscribers.insert({
                    email: email,
                    verifyId: verifyId
                });
                subscribeUrl = Meteor.absoluteUrl("subscribe/" + verifyId);
            }
            var emailTemplate = Meteor.call('renderTemplate', 'share-post.html', {
                email: email,
                url: Meteor.absoluteUrl("posts/" + postId),
                name: currentUser.profile.name,
                title: title,
                subscribeUrl: subscribeUrl
            });
            Meteor.call("sendEmail", email, "support@fits.io", subject, emailTemplate);
        },
        sendEmail: function (to, from, subject, text) {
            check([to, from, subject, text], [String]);
            this.unblock();
            Email.send({
                to: to,
                from: from,
                subject: subject,
                html: text
            });
        },
        notifySubscribers: function (postId, host) {
            var subscribers = SubscribedEmails.find().fetch();
            var post = Post.findOne({_id: postId});
            for (var subscriber in subscribers) {
                var postUrl = host + "/posts/" + postId;
                var subject = post.title + " on fits.io";
                var emailTemplate = Meteor.call('renderTemplate', 'notify-subscribers.html', {
                    email: subscribers[subscriber].email,
                    title: post.title,
                    postUrl: postUrl
                });
                Meteor.call("sendEmail", subscribers[subscriber].email, "support@fits.io", subject, emailTemplate);
            }
        },
        sendWelcomeEmail: function (user) {
            Meteor.users.findOne(user._id);
            var verifyUrl = Meteor.absoluteUrl("users/" + user._id + "/verify-account/" + user.profile.verifyId);
            var emailTemplate = Meteor.call('renderTemplate', 'welcome-email.html', {
                name: user.profile.name != "" ? user.profile.name : null,
                verifyUrl: verifyUrl
            });
            Meteor.call("sendEmail", user.emails[0].address, "support@fits.io", "Welcome aboard, team matey!", emailTemplate);
        },
        verifyAccount: function(userId, verifyId) {
            var user = Meteor.users.findOne(userId);
            if(user && !user.emails[0].verified && user.profile.verifyId == verifyId) {
                Meteor.users.update({_id: userId}, {$set: {"emails.0.verified": true}});
                Meteor.users.update({_id: userId}, {$unset: {"profile.verifyId": ""}});
                return true;
            } else {
                return false;
            }
        },
        renderTemplate: function (templateName, content) {
            SSR.compileTemplate('template', Assets.getText(templateName));
            return SSR.render('template', content);
        },
        getServerTime: function () {
            return (new Date()).getTime();
        },
        checkUserPassword: function (userPassword) {
            var user = Meteor.user();
            check(userPassword, String);
            var digest = Package.sha.SHA256(userPassword);
            var password = {digest: digest, algorithm: 'sha-256'};
            var result = Accounts._checkPassword(user, password);
            if (result.error == null) {
                return true;
            } else {
                return false;
            }
        },
        checkUserEmail: function (email) {
            var users = Meteor.users.find({"emails.address": email, _id: {$ne: this.userId}}).count();
            if (!users) {
                return true;
            } else {
                return false;
            }
        },
        sendVerificationMessage : function() {
            var verifyId = Random.id(100);
            var user = Meteor.user();
            Meteor.users.update(this.userId, {
                $set: {
                    "profile.verifyId": verifyId
                }
            });
            var verifyUrl = Meteor.absoluteUrl("users/" + user._id + "/verify-new-email/" + verifyId);
            var emailTemplate = Meteor.call('renderTemplate', 'verify-new-email.html', {
                name: user.profile.name != "" ? user.profile.name : null,
                verifyUrl: verifyUrl
            });
            Meteor.call("sendEmail", user.profile.unverifiedEmail, "support@fits.io", "Please, verify your new email address!!", emailTemplate);
        },
        verifyNewEmail: function(userId, verifyId) {
            var user = Meteor.users.findOne(userId);
            if(user && user.profile.verifyId == verifyId) {
                Meteor.users.update({_id: userId}, {$set: {"emails.0.address": user.profile.unverifiedEmail}});
                Meteor.users.update({_id: userId}, {$unset: {"profile.unverifiedEmail": "", "profile.verifyId": ""}});
                return true;
            } else {
                return false;
            }
        },
        sendPasswordRestoreMessage: function(email) {
            var user = Meteor.users.findOne({"emails.0.address": email});
            var verifyId = Random.id(100);
            if(user) {
                Meteor.users.update({_id: user._id}, {$set: {"profile.passwordVerifyId": verifyId}});
                var verifyUrl = Meteor.absoluteUrl("users/" + user._id + "/verify-password/" + verifyId);
                var emailTemplate = Meteor.call('renderTemplate', 'verify-new-password.html', {
                    name: user.profile.name != "" ? user.profile.name : null,
                    verifyUrl: verifyUrl
                });
                Meteor.call("sendEmail", user.emails[0].address, "support@fits.io", "Please, change your password!!", emailTemplate);
                return true;
            } else {
                return false;
            }
        },
        setUpNewPassword: function (userId, verifyId, password) {
            var user = Meteor.users.findOne(userId);
            if(user && user.profile.passwordVerifyId == verifyId) {
                Accounts.setPassword(userId, password, function (error) {
                    if(error) {
                        console.log(error);
                    }
                });
                return true;
            } else {
                return false;
            }
        }
    });

    Accounts.config({
        forbidClientAccountCreation: true
    });

    Accounts.onCreateUser(function (options, user) {
        var image = "/user.png";
        if (user.services.facebook != undefined) {
            image = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
        }
        if (user.services.twitter != undefined) {
            image = user.services.twitter.profile_image_url;
        }
        if (user.services.instagram != undefined) {
            image = user.services.instagram.profile_picture;
        }
        if (options.profile) {
            if (!options.profile.thumbnail) {
                options.profile.thumbnail = image;
            }
            user.profile = options.profile;
            user.roles = ['general'];
        }
        if(user.services.instagram == undefined && user.services.facebook == undefined && user.services.twitter == undefined) {
            user.profile.verifyId = Random.id(100);
            Meteor.call("sendWelcomeEmail", user);
        }
        return user;
    });

    Accounts.validateLoginAttempt(function (data) {
        if (data.error) {
            console.log(data.error);
            return false;
        } else {
            if(data.type == "password") {
                return true;
            } else {
                return data.type == "instagram" || data.type == "facebook" || data.type == "twitter" || data.type == "resume";
            }
        }
    });

    Meteor.publish('Roles', function () {
        return Meteor.roles.find({});
    });
    Meteor.publish('Post', function () {
        return Post.find({});
    });
    Meteor.publish('SubscribedEmails', function () {
        if (Roles.userIsInRole(this.userId, 'super-admin')) {
            return SubscribedEmails.find({});
        } else {
            return null;
        }
    });
    Meteor.publish('UnderModerationPosts', function () {
        return UnderModeration.find({});
    });
    Meteor.publish('Category', function () {
        return Category.find({});
    });
    Meteor.publish('AllUsers', function () {
        return Meteor.users.find({}, {
            fields: {
                'profile.name': 1,
                'profile.thumbnail': 1,
                'emails': 1,
                'roles': 1,
                'services': 1
            }
        });
    });
    Meteor.publish('activeUser', function () {
        return Meteor.users.find({_id: this.userId}, {
            fields: {
                'roles': 1
            }
        });
    });

    Meteor.users.allow({
        remove: function (userId, userToRemove) {
            if (Roles.userIsInRole(userId, 'super-admin') && userId != userToRemove._id) {
                return true;
            } else {
                return false;
            }
        },
        update: function (userId, userToUpdate) {
            if (Roles.userIsInRole(userId, 'super-admin') || (userId == userToUpdate._id)) {
                return true;
            } else {
                return false;
            }
        }
    });

    ServiceConfiguration.configurations.remove({
        service: 'facebook'
    });
    ServiceConfiguration.configurations.remove({
        service: 'twitter'
    });
    ServiceConfiguration.configurations.remove({
        service: "instagram"
    });
    ServiceConfiguration.configurations.insert({
        service: "facebook",
        appId: "950217145031202",
        secret: "7d81da3216b4801643c9aed18dff0b93"
    });
    ServiceConfiguration.configurations.insert({
        service: "twitter",
        consumerKey: "acpyan2RAvDnOLpd4DIQRpNvm",
        secret: "4az9o9r6oNA26kd1rz8ULdPeG1Vew4fMZNZN0JWcfDoPYTtknG"
    });
    ServiceConfiguration.configurations.insert({
        service: "instagram",
        clientId: "8d6bbf15869e49709dbd4afbd2d21693",
        scope: 'basic',
        secret: "5cf921748d2d45b697a02f1496aeaad7"
    });
});
