Original = new FS.Collection("original", {
    stores: [
        new FS.Store.FileSystem("original", {
            transformWrite: function(fileObj, readStream, writeStream) {
                readStream.pipe(writeStream);
            }
        }),
        new FS.Store.FileSystem("thumb", {
            transformWrite: function (fileObj, readStream, writeStream) {
                if (gm.isAvailable) {
                    gm(readStream, fileObj.name()).autoOrient().resize('300', '200', "^").gravity('Center').extent('300', '200').stream().pipe(writeStream);
                }
            }
        }),
        new FS.Store.FileSystem("small", {
            transformWrite: function (fileObj, readStream, writeStream) {
                if (gm.isAvailable) {
                    gm(readStream, fileObj.name()).autoOrient().resize('150', '150', "^").gravity('Center').extent('150', '150').stream().pipe(writeStream);
                }
            }
        })
    ],
    filter: {
        allow: {
            contentTypes: ['image/*']
        }
    }
});

Original.allow({
    download: function () {
        return true;
    },
    insert: function () {
        return true;
    },
    update: function (userId) {
        return userId ? true : false;
    },
    remove: function (userId) {
        return userId ? true : false;
    }
});

Meteor.publish('Original', function () {
    return Original.find({});
});