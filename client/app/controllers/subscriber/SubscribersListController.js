angular.module("mobile-events").controller("SubscribersListCtrl", ['$scope', '$collection',
    function ($scope, $collection) {
        $collection(SubscribedEmails).bind($scope, 'subscribers');

        $scope.removeSubscriber = function (subscriber) {
            Meteor.call("removeSubscriber", subscriber._id);
        };
    }]);
