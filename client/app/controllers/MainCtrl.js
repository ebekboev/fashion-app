angular.module("mobile-events").controller("MainCtrl", ['$scope', '$rootScope', '$collection', '$location', '$timeout',
    function ($scope, $rootScope, $collection, $location, $timeout) {
        $collection(Category).bind($scope, 'categories');
        $collection(Post).bind($scope, 'originalPosts');
        $scope.postsToShow = 1;

        $scope.tag = null;
        if (!angular.equals($location.search(), {})) {
            $scope.tag = $location.search().tag;
        }

        $scope.loadMore = function () {
            console.log('loading...');
        };

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (toState.name == 'home.post') {
                $rootScope.previusPage = "home";
                $rootScope.searchQuery = $location.search();
            }
        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (fromState.name == 'home.post' && !angular.equals($rootScope.searchQuery, {})) {
                $location.search($rootScope.searchQuery);
                $scope.tag = $location.search().tag;
            }
        });

        $scope.goHome = function () {
            $scope.tag = null;
            $scope.categoryFilter = [];
        };

        $scope.$watch('originalPosts', function (posts) {
            Meteor.call('getPostsCount', function (error, count) {
                Meteor.call('getServerTime', function (error, date) {
                    $scope.today = date;
                    if (posts.length == count) {
                        $scope.sortedPosts = {};
                        $scope.readyToUsePosts = [];
                        for (var index = 0; index < posts.length; index++) {
                            var date = moment(posts[index].created).format("MM-DD-YYYY");
                            var id = moment(date, "MM-DD-YYYY").format('x');
                            if ($scope.sortedPosts[id]) {
                                $scope.sortedPosts[id].posts.push(posts[index]);
                            } else {
                                $scope.sortedPosts[id] = {};
                                $scope.sortedPosts[id].created = id;
                                $scope.sortedPosts[id].posts = [];
                                $scope.sortedPosts[id].posts.push(posts[index]);
                            }
                        }
                        for (var post in $scope.sortedPosts) {
                            $scope.readyToUsePosts.push($scope.sortedPosts[post]);
                            $scope.$apply();
                        }
                    }
                });
            });
        });

        $scope.getWeekDay = function (date) {
            return moment(date, "x").format("dddd");
        };

        $scope.getMonth = function (date) {
            return moment(date, "x").format("MMMM");
        };

        $scope.getDay = function (date) {
            return moment(date, "x").format("Do");
        };

        $scope.getYear = function (date) {
            return moment(date, "x").format("YYYY");
        };

        $scope.isToday = function (date) {
            return moment(date, "x").format("DD-MM-YYYY") == moment($scope.today, "x").format("DD-MM-YYYY") ?
                true : false;
        };

        $scope.newUser = {
            email: "",
            password: "",
            name: ""
        };

        $scope.newPost = {
            title: "",
            category: "",
            tags: [],
            image: {
                small: null,
                thumb: null,
                original: null
            },
            author: "",
            votes: [],
            comments: [],
            source: "",
            created: ""
        };

        $scope.subscribe = function (email) {
            if (email) {
                if (checkEmail(email)) {
                    $('#submit-subscription').attr('disabled', true);
                    Meteor.call('subscribe', email, $location.host(), function (error, message) {
                        if (message == "Already subscribed!") {
                            $('#subscription-confirmation').modal('show');
                            $('#submit-subscription').attr('disabled', false);
                        } else {
                            $('.subscription').hide(500);
                        }
                    });
                } else {
                    return;
                }
            } else {
                return;
            }
        };

        $scope.hideSubscriptionForm = function () {
            $('.subscription').hide(500);
        };

        $scope.vote = function (postId) {
            Meteor.call('voteForPost', postId);
        };

        $scope.getVotesCounts = function (post) {
            return post.votes.length * -1;
        };

        $scope.addPost = function () {
            var file = $scope.thumbnail;
            if (!file.type.match('image.*')) {
                alert('Select an image, please!');
                return;
            } else {
                $('body').addClass("loading");
                Original.insert(file, function (err, fileObj) {
                    Deps.autorun(function (computation) {
                        var file = Original.findOne(fileObj._id);
                        if (file.hasStored('small') && file.hasStored('thumb') && file.hasStored('original')) {
                            $scope.newPost.image.small = "/cfs/files/original/" + fileObj._id + "?store=small";
                            $scope.newPost.image.thumb = "/cfs/files/original/" + fileObj._id + "?store=thumb";
                            $scope.newPost.image.original = "/cfs/files/original/" + fileObj._id + "?store=original";
                            $scope.newPost.category = $scope.newPost.category._id;
                            $scope.newPost.author = $rootScope.currentUser._id;
                            $scope.newPost.tags = $('#id_tags').tagsinput('items');
                            Meteor.call('getServerTime', function (err, date) {
                                $scope.newPost.created = date;
                                UnderModeration.insert($scope.newPost);
                                $scope.newPost = {
                                    title: "",
                                    category: "",
                                    tags: [],
                                    image: {
                                        small: null,
                                        thumb: null,
                                        original: null
                                    },
                                    author: "",
                                    votes: [],
                                    comments: [],
                                    source: ""
                                };
                                $scope.thumbnail = null;
                                $('#post-image').val("");
                                $('#id_tags').tagsinput('removeAll');
                                $('body').removeClass("loading");
                                $("#modalAddPeople").modal('hide');
                                $('#popup-confirmation').modal('show');
                                computation.stop();
                            });
                        }
                    });
                });
            }

        };

        $scope.isInputsValid = function () {
            return $scope.newPost.category == '' || $scope.newPost.title == '' || $scope.thumbnail == null ||
                $scope.newPost.source == '' || $('#id_tags').tagsinput('items').length < 1;
        };

        $scope.method = "pc";

        $scope.hide = function (source) {
            $(source).hide();
        };

        $scope.signUp = function () {
            $('body').addClass("loading");
            if (checkUserInputs()) {
                Meteor.call('createNewUser', $scope.newUser.email, $scope.newUser.password, $scope.newUser.name, function (err) {
                    if (err) {
                        $("#duplicateEmail").show();
                        $('body').removeClass("loading");
                    } else {
                        $("#duplicateEmail").hide();
                        $("#modalSignIn").modal('hide');
                        $('body').removeClass("loading");
                        $('#verify-confirmation').modal('show');
                        resetModel();
                    }
                });
            }
        };

        var checkUserInputs = function () {
            if ($scope.newUser.password == $scope.newUser.repeatPassword) {
                $('#passwordsMismatch').hide();
                if (checkEmail($scope.newUser.email)) {
                    $('#incorrectEmailFormat').hide();
                    return true;
                } else {
                    $('#incorrectEmailFormat').show();
                    $('body').removeClass("loading");
                }
            } else {
                $('#passwordsMismatch').show();
                $('body').removeClass("loading");
            }
        };

        var checkEmail = function (email) {
            var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return regex.test(email);
        };

        $scope.signIn = function () {
            $('body').addClass("loading");
            Meteor.loginWithPassword($scope.email, $scope.password, function (error) {
                setTimeout(function () {
                    if (error) {
                        $("#nonExistantUser").show();
                    } else {
                        $scope.email = "";
                        $scope.password = "";
                        $("#nonExistantUser").hide();
                        $('.modal').modal('hide');
                    }
                    $('body').removeClass("loading");
                }, 500);
            });
        };

        $scope.forgotPassword = function () {
            $location.path("/forgot-password");
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        };

        $scope.isExistantUserInputsEmpty = function () {
            return !$scope.password || !$scope.email;
        };

        $scope.isNewUserInputsEmpty = function () {
            return !$scope.newUser.email || !$scope.newUser.repeatPassword || !$scope.newUser.password || !$scope.newUser.name;
        };

        $scope.logIn = function () {
            $("#modalSignIn").modal('show');
        };

        $scope.loginWith = function (social) {
            $('body').addClass("loading");
            switch (social) {
                case 'twitter' :
                    Meteor.loginWithTwitter({}, function (err) {
                        if (err) {
                            console.log(err);
                        }
                        $('body').removeClass("loading");
                        $("#modalSignIn").modal('hide');
                        $("#modalSignUp").modal('hide');
                    });
                    break;
                case 'facebook':
                    Meteor.loginWithFacebook({requestedPermissions: ['email', 'public-profile']}, function (err) {
                        if (err) {
                            console.log(err);
                        }
                        $('body').removeClass("loading");
                        $("#modalSignIn").modal('hide');
                        $("#modalSignUp").modal('hide');
                    });
                    break;
                case 'instagram':
                    Meteor.loginWithInstagram(function (err, res) {
                        if (err == undefined) {

                        }
                        $("#modalSignIn").modal('hide');
                        $("#modalSignUp").modal('hide');
                        $('body').removeClass("loading");
                    });
                    break;
                default:
                    $("#modalSignIn").modal('hide');
                    $("#modalSignUp").modal('hide');
                    $('body').removeClass("loading");
                    break;
            }
        };

        $scope.logOut = function () {
            $('body').addClass("loading");
            Meteor.logout(function (err) {
                if (err) {
                    console.log(err);
                }
                $('body').removeClass("loading");
            });
        };

        $scope.changeProfile = function () {
            $rootScope.$broadcast("current-user", $rootScope.currentUser);
        };

        var resetModel = function () {
            $scope.newUser = {
                email: "",
                password: "",
                name: ""
            };
        };

        $scope.getLastCommenter = function (post) {
            var outer = post.comments.length;
            if (outer > 0) {
                var inner = post.comments[outer - 1].comments.length;
                if (inner > 0) {
                    return Meteor.users.findOne(post.comments[outer - 1].comments[inner - 1].author).profile;
                } else {
                    return Meteor.users.findOne(post.comments[outer - 1].author).profile;
                }
            } else {
                return Meteor.users.findOne(post.author).profile;
            }
        };

        $scope.getCommentsCount = function (comments) {
            var total = comments.length;
            for (var comment in comments) {
                total += comments[comment].comments.length;
            }
            return total;
        };

        $scope.getUsername = function (id) {
            if (!$rootScope.loggingIn) {
                return Meteor.users.findOne(id).profile.name;
            }
        };

        $scope.categoryFilter = [];

        $scope.filterByCategory = function (category) {
            var index = $scope.categoryFilter.indexOf(category._id);
            if (index > -1) {
                $scope.categoryFilter.splice(index, 1);
            } else {
                $scope.categoryFilter.push(category._id);
            }
        };

        $scope.isCategoryDisabled = function (category) {
            var index = $scope.categoryFilter.indexOf(category);
            if (index > -1) {
                return false;
            } else {
                return true;
            }
        };

        $scope.isPostOutOfCategory = function (post) {
            var index = $scope.categoryFilter.indexOf(post.category);
            if (index > -1) {
                return false;
            } else {
                return true;
            }
        };

        $scope.isUserNonSocial = function () {
            if ($rootScope.currentUser && $rootScope.currentUser.services) {
                if ($rootScope.currentUser.services.hasOwnProperty('facebook') ||
                    $rootScope.currentUser.services.hasOwnProperty('twitter') ||
                    $rootScope.currentUser.services.hasOwnProperty('instagram')) {
                    return false;
                } else {
                    return true;
                }
            }
        };

        $scope.isPostFilteredByTag = function (post) {
            if ($scope.tag) {
                var index = post.tags.indexOf($scope.tag);
                if (index > -1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        };

        $scope.filterByTag = function (tag, $event) {
            $event.stopPropagation();
            $location.search({tag: tag});
            $scope.tag = tag;
            return false;
        };

        $scope.isAdmin = function () {
            return Roles.userIsInRole(Meteor.userId(), 'super-admin');
        };

        $scope.isModerator = function () {
            return Roles.userIsInRole(Meteor.userId(), 'moderator');
        };

        $scope.initialize = function () {
            $('#id_tags').tagsinput();
            $('.people-item-title').unbind('click').on('click', function (e) {
                e.preventDefault();
                window.open($(this).attr('href'), '_blank');
                return false;
            });
        };

        $('#bottom-indicator').bind('inview', function (event, isInView, visible) {
            console.log(isInView);
            if (isInView) {
                $timeout(function () {
                    $scope.postsToShow += 1;
                    $scope.bottomIsVisible = true;
                    $scope.$apply();
                    $('#ajax-loader').hide();
                    if ($scope.readyToUsePosts && $scope.postsToShow >= $scope.readyToUsePosts.length) {
                        $('#ajax-loader').hide();
                    } else {
                        $timeout(function () {
                            $('#ajax-loader').show();
                        }, 400);
                    }
                }, 400);
            }
        });
    }]);
