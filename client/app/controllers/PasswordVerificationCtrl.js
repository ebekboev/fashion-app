angular.module("mobile-events").controller("PasswordVerificationCtrl", ['$scope', '$stateParams', '$location', '$timeout',
    function ($scope, $stateParams, $location, $timeout) {
        $scope.setUpNewPassword = function () {
            if ($scope.newPassword && $scope.newPassword == $scope.newPasswordRepeat) {
                Meteor.call('setUpNewPassword', $stateParams.userId, $stateParams.verifyId, $scope.newPassword, function (error, status) {
                    if (error) {
                        console.log(error);
                    }
                    console.log(status);
                    if (status) {
                        $scope.message = "Your have successfully changed your password. Please Sign In.";
                        $scope.$apply();
                        $('#setupComplete').modal('show');
                        $timeout(function () {
                            $('#setupComplete').modal('hide');
                            $timeout(function () {
                                $location.path("/");
                            }, 1000);
                        }, 3000);
                    } else {
                        $scope.message = "User not found. Redirecting...";
                        $scope.$apply();
                        $('#setupComplete').modal('show');
                        $timeout(function () {
                            $('#setupComplete').modal('hide');
                            $timeout(function () {
                                $location.path("/");
                            }, 1000);
                        }, 3000);
                    }
                });
            } else {
                $('#password-restore-error').show();
            }
        };

        $scope.isInputsValid = function () {
            return $scope.newPassword && $scope.newPasswordRepeat;
        };
    }]);
