angular.module("mobile-events").controller("ArtistsListCtrl", ['$scope', '$collection',
	function ($scope, $collection) {
		$collection(Category).bind($scope, 'categories');

		$scope.removeCategory = function (categoryId) {
			Category.remove({_id: categoryId});
		};
	}]);
