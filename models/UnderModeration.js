UnderModeration = new Mongo.Collection('UnderModeration');

UnderModeration.allow({
    insert: function (userId) {
        return userId ? true : false;
    },
    update: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin', 'moderator']) ? true : false;
    },
    remove: function (userId) {
        return Roles.userIsInRole(userId, ['super-admin', 'moderator']) ? true : false;
    }
});