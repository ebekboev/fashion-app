angular.module("mobile-events").controller("PostDetailsCtrl", ['$scope', '$rootScope', '$state', '$collection', '$stateParams', '$location', '$timeout',
    function ($scope, $rootScope, $state, $collection, $stateParams, $location, $timeout) {
        $scope.votedUsers = {};
        var slickConfig = {
            variableWidth: true,
            dots: false,
            infinite: false,
            slidesToShow: 8,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 3,
                        arrows: false
                    }
                }
            ]
        };

        $scope.votedUser = null;

        $scope.goHome = function (tag) {
            $rootScope.searchQuery = {tag: tag};
            $state.go('home');
        };

        $scope.showPopup = function () {
            if(!$('#modalPeopleDetail').is(':visible')) {
                $('#votes-users, .post-item-votes-count').hide();
            }
            $('#modalPeopleDetail').on('shown.bs.modal', function () {
                if ($rootScope.previusPage == "home") {
                    if ($scope.post.votes.length < 8) {
                        if (window.innerWidth <= 480 && $scope.post.votes.length > 3) {
                            !$('#votes-users').hasClass('slick-slider') ? $('#votes-users').slick(slickConfig) : "";
                            $('#votes-users, .post-item-votes-count').show();
                        } else {
                            $('.people-thumb').css('margin', '0 5px');
                            $('#votes-users, .post-item-votes-count').show();
                        }
                    } else {
                        !$('#votes-users').hasClass('slick-slider') ? $('#votes-users').slick(slickConfig) : "";
                        $('#votes-users, .post-item-votes-count').show();
                    }
                } else {
                    $timeout(function () {
                        if ($scope.post.votes.length < 8) {
                            if (window.innerWidth <= 480 && $scope.post.votes.length > 3) {
                                !$('#votes-users').hasClass('slick-slider') ? $('#votes-users').slick(slickConfig) : "";
                            } else {
                                $('.people-thumb').css('margin', '0 5px');
                            }
                            $('#votes-users, .post-item-votes-count').show();
                        } else {
                            !$('#votes-users').hasClass('slick-slider') ? $('#votes-users').slick(slickConfig) : "";
                            $('#votes-users, .post-item-votes-count').show();
                        }
                    }, 1000);
                }
            });
            $('#modalPeopleDetail').modal('show');
            $('#modalPeopleDetail').on('hidden.bs.modal', function () {
                $state.go('home');
            });
            $('#shareLink').on('hidden.bs.modal', function () {
                $('body').addClass("modal-open");
            });
        };

        $scope.showLink = function() {
            $('#shareLink').modal('show');
            $('#sharing-link').val($location.absUrl());
        };

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (toState.name == 'home') {
                $('#modalPeopleDetail').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            }
        });

        $collection(Post).bindOne($scope, "post", $stateParams.id);
        $scope.$watch('post', function (post) {
            if (post) {
                $collection(Meteor.users).bindOne($scope, "author", post.author);
                $scope.created = difference(new Date(), post.created);
                $scope.socials = {
                    facebook: "https://www.facebook.com/dialog/feed?app_id=" + $scope.getSocialId('facebook').fetch()[0].appId + "&display=popup&caption=" + post.title + "&link=" + $location.absUrl() + "&redirect_uri=" + [[$location.protocol() + "://" + $location.host()]],
                    twitter: "https://twitter.com/intent/tweet?text=" + post.title + "&url=" + $location.absUrl()
                };
                $('#shareViaEmail').on('hidden.bs.modal', function () {
                    $('#sharing-email').css('border-color', '#ccc');
                });
                $scope.showPopup();
            } else {
                $location.path("/");
            }
        });

        $scope.vote = function (postId) {
            Meteor.call('voteForPost', postId);
        };

        $scope.emailChanged = function () {
            $('#sharing-email').css('border-color', '#ccc');
        };

        $scope.sharePostViaEmail = function (email) {
            if (checkEmail(email)) {
                $scope.emailToShare = "";
                $('#sharing-email').css('border-color', '#ccc');
                Meteor.call('sharePostViaEmail', email, $scope.post._id, function (error, result) {
                    error ? console.log(error) : null;
                    $('#sharing-email').css('border-color', '#ccc');
                    $('#shareViaEmail').modal('hide');
                });
            } else {
                $('#sharing-email').css('border-color', 'red');
            }
        };

        var checkEmail = function (email) {
            var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return regex.test(email);
        };

        $scope.getSocialId = function (social) {
            return ServiceConfiguration.configurations.find({service: social});
        };

        $scope.getUserThumbnail = function (id) {
            if ($scope.votedUsers[id]) {
                return $scope.votedUsers[id].profile.thumbnail;
            } else {
                $scope.votedUsers[id] = Meteor.users.findOne(id);
                return $scope.votedUsers[id].profile.thumbnail;
            }
        };

        $scope.getUserName = function (id) {
            if ($scope.votedUsers[id]) {
                return $scope.votedUsers[id].profile.name;
            } else {
                $scope.votedUsers[id] = Meteor.users.findOne(id);
                return $scope.votedUsers[id].profile.name;
            }
        };

        $scope.addComment = function (parentIndex, childIndex) {
            var message, target;
            if (childIndex > -1) {
                message = $('#message_' + parentIndex + '_' + childIndex).val();
                target = $scope.post.comments[parentIndex].comments[childIndex];
            } else {
                message = $('#message_' + parentIndex).val();
                target = $scope.post.comments[parentIndex];
            }
            if (message) {
                Meteor.call('addPostComment', $scope.post._id, parentIndex, childIndex, target, message, function (err, status) {
                    $('.comment-reply').hide();
                });
            }
        };

        $scope.addRootComment = function () {
            var message = $('#new-message').val();
            if (message) {
                Meteor.call('addPostComment', $scope.post._id, null, null, null, message, function (err, status) {
                    $('#new-message').val("");
                });
            }
        };

        $scope.reply = function (id) {
            $('.comment-reply').hide();
            $('#reply_' + id).toggle();
        };

        $scope.cancelReply = function (parentIndex, childIndex) {
            if (childIndex > -1) {
                $('#reply_' + parentIndex + '_' + childIndex).hide();
            } else {
                $('#reply_' + parentIndex).hide();
            }
        };

        $scope.getDate = function (date) {
            return difference(new Date(), date);
        };

        function difference(d1, d2) {
            var m = moment(d1);
            var years = m.diff(d2, 'years');
            m.add(-years, 'years');
            var months = m.diff(d2, 'months');
            m.add(-months, 'months');
            var weeks = m.diff(d2, 'weeks');
            m.add(-weeks, 'weeks');
            var days = m.diff(d2, 'days');
            m.add(-days, 'days');
            var hours = m.diff(d2, 'hours');
            m.add(-hours, 'hours');
            var minutes = m.diff(d2, 'minutes');
            m.add(-minutes, 'minutes');
            return {years: years, months: months, weeks: weeks, days: days, hours: hours, minutes: minutes};
        }
    }])
;
