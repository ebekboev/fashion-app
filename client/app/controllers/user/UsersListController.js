angular.module("mobile-events").controller("UsersListCtrl", ['$scope', '$collection',
    function ($scope, $collection) {
        $collection(Meteor.users).bind($scope, 'users');

        $scope.isSuperAdmin = function(user) {
            return Roles.userIsInRole(user._id, "super-admin");
        };

        $scope.removeUser = function (user) {
            Meteor.call("removeUser", user._id);
        };

        $scope.setAsModerator = function (id) {
            Meteor.call("setAsModerator", id);
        };

        $scope.setAsGeneralUser = function (id) {
            Meteor.call("setAsGeneralUser", id);
        };

        $scope.toggleModeratorRights = function (user) {
            if(user.roles.indexOf('moderator') > -1) {
                $scope.setAsGeneralUser(user._id);
            } else {
                $scope.setAsModerator(user._id);
            }
        };
    }]);
